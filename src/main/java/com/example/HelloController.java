package com.example;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kevin 
 */

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Bienvenidos al mundo de la integracion continua";
    }

}
